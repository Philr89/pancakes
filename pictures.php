<?php

require_once 'vendor/autoload.php';



use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;

// All templates will be given userSession variable
$container['view']->getEnvironment()->addGlobal('userSession', $_SESSION['user'] ?? null);
$container['view']->getEnvironment()->addGlobal('flashMessage', getAndClearFlashMessage());
$container['view']->getEnvironment()->addGlobal('picture', convertPicture());
$container['upload_directory'] = __DIR__ . '/uploads';


$app->get('/addpicture', function (Request $request, Response $response, $args) {
    if (!isset($_SESSION['user'])) { // refuse if user not logged in
        $response = $response->withStatus(403);
        return $this->view->render($response, 'error_access_denied.html.twig');
    }
    return $this->view->render($response, 'addpicture.html.twig');
});

$app->post('/addpicture', function (Request $request, Response $response, $args) {
    $title = $request->getParam('title');

    //DECLARE THE ERROR LIST
    $errorList = array();

    $hasPhoto = false;
    $uploadedImage = $request->getUploadedFiles()['image'];
    if ($uploadedImage->getError() != UPLOAD_ERR_NO_FILE) { // was anything uploaded?
        // print_r($uploadedImage->getError());
        $hasPhoto = true;
        $result = verifyUploadedPhoto($uploadedImage);
        if ($result !== TRUE) {
            $errorList[] = $result;
        }
    }

    if ($errorList) {
        return $this->view->render(
            $response,
            'addpicture.html.twig',
            ['errorList' => $errorList, 'v' => ['title' => $title]]
        );
    } else {
        if ($hasPhoto) {
            $directory = $this->get('upload_directory');
            $uploadedImagePath = moveUploadedFile($directory, $uploadedImage);
            if ($uploadedImagePath == FALSE) {
                return $response->withRedirect("/internalerror", 301);
            }
        }
        $userId = $_SESSION['user']['id'];
        DB::insert('pictures', ['id' => $userId, 'title' => $title, 'image' => $uploadedImagePath,]);
        $userId = DB::insertId();
        setFlashMessage("Image stored successfully");
        return $response->withRedirect("/addpicture");
    }
});




////////////////////////////////////
// THE FUNCTIONS
///////////////////////////////////

function convertPicture()
{
    $picture = null;
    $convertedPicture = ($_SESSION['user']['picture'] ??  null);
    if (!empty($convertedPicture)) {
        $picture = base64_encode($convertedPicture);
    }
    return $picture;
}

function verifyUploadedPhoto(Psr\Http\Message\UploadedFileInterface $photo, &$mime = null)
{
    if ($photo->getError() != 0) {
        return "Error uploading photo " . $photo->getError();
    }
    // if ($photo->getSize() > 1024 * 1024) { // 1MiB
    //     return "File too big. 1MB max is allowed.";
    // }
    $info = getimagesize($photo->file);
    if (!$info) {
        return "File is not an image";
    }
    // echo "\n\nimage info\n";
    // print_r($info);
    // if ($info[0] < 200 || $info[0] > 1000 || $info[1] < 200 || $info[1] > 1000) {
    //     return "Width and height must be within 200-1000 pixels range";
    // }
    $ext = "";
    switch ($info['mime']) {
        case 'image/jpeg':
            $ext = "jpg";
            break;
        case 'image/gif':
            $ext = "gif";
            break;
        case 'image/png':
            $ext = "png";
            break;
        default:
            return "Only JPG, GIF and PNG file types are allowed";
    }
    if (!is_null($mime)) {
        $mime = $info['mime'];
    }
    return TRUE;
}

function moveUploadedFile($directory, UploadedFile $uploadedFile)
{
    // Avoid a serious security flaw - user must not be ablet o upload .php file and exploit our server
    $extension = strtolower(pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION));
    if (!in_array($extension, ['jpg', 'jpeg', 'gif', 'png'])) {
        return FALSE;
    }
    $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
    $filename = sprintf('%s.%0.8s', $basename, $extension);
    try {
        $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename); // FIXME catch exception on failure
    } catch (Exception $e) {
        // TODO: log the error message
        return FALSE;
    }
    return $filename;
}
