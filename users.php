<?php

require_once 'vendor/autoload.php';

use Slim\Http\Request;
use Slim\Http\Response;

// All templates will be given userSession variable
$container['view']->getEnvironment()->addGlobal('userSession', $_SESSION['user'] ?? null);
$container['view']->getEnvironment()->addGlobal('flashMessage', getAndClearFlashMessage());

$passwordPepper = 'mmyb7oSAeXG9DTz2uFqu';

// Register
$app->get('/register', function (Request $request, Response $response, $args) {
    return $this->view->render($response, 'register.html.twig', ['userSession' => $_SESSION['user'] ?? null]);
});

$app->post('/register', function ($request, $response, $args) {
    $username = $request->getParam('username');
    $email = $request->getParam('email');
    $pass1 = $request->getParam('pass1');
    $pass2 = $request->getParam('pass2');

    //DECLARE THE ERROR LIST
    $errorList = array();

    // Verify the username is available
    $result = verifyUserName($username);
    if ($result !== TRUE) {
        $errorList[] = $result;
    }

    ////////////////////////////////////////////////////
    // Verify image ** ONLY IF THERE IS AN IMAGE IN EXAM
    // $hasPhoto = false;
    // $mimeType = "";
    // $uploadedImage = $request->getUploadedFiles()['image'];
    // if ($uploadedImage->getError() != UPLOAD_ERR_NO_FILE) { // was anything uploaded?
    //     // print_r($uploadedImage->getError());
    //     $hasPhoto = true;
    //     $result = verifyUploadedPhoto($uploadedImage, $mimeType);
    //     if ($result !== TRUE) {
    //         $errorList[] = $result;
    //     }
    // }
    ////////////////////////////////////////////////////

    // Verify email
    if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
        $errorList[] =  "Email does not look valid";
        $email = "";
    } else {
        // is email already in use?
        $record = DB::queryFirstRow("SELECT id FROM users WHERE email=%s", $email);
        if ($record) {
            array_push($errorList, "This email is already registered");
            $email = "";
        }
    }

    // Verify password
    $result = verifyPasswordQuailty($pass1, $pass2);
    if ($result != TRUE) {
        $errorList[] = $result;
    }

    if ($errorList) { // STATE 3: errors
        return $this->view->render(
            $response,
            'register.html.twig',
            ['errorList' => $errorList, 'v' => ['username' => $username, 'email' => $email]]
        );
    } else { // STATE 2: all good
        //ONLY MATTERS IF THERE IS AN IMAGE
        // $photoData = null;
        // if ($hasPhoto) {
        //     $photoData = file_get_contents($uploadedImage->file);
        // }
        ///////////////////////////

        global $passwordPepper;
        $pwdPeppered = hash_hmac("sha256", $pass1, $passwordPepper);
        $pwdHashed = password_hash($pwdPeppered, PASSWORD_DEFAULT); // PASSWORD_ARGON2ID);
        DB::insert('users', [
            'username' => $username, 'email' => $email, 'password' => $pwdHashed
        ]);

        // IF THERE NEEDS TO BE AN IMAGE, USE THIS ONE INSTEAD
        // DB::insert('users', [
        //     'username' => $username, 'email' => $email, 'password' => $pwdHashed,
        //     'imageData' => $photoData, 'imageMimeType' => $mimeType
        // ]);
        setFlashMessage("You've been registered. Please login now");
        return $response->withRedirect('/login');
    }
});


////////////////////////////////////////////////////////////////////////////////
// Login
///////////////////////////////////////////////////////////////////////////////
$app->get('/login[/{params:.*}]', function (Request $request, Response $response, $args) {
    return $this->view->render($response, 'login.html.twig');
});

$app->post('/login', function (Request $request, Response $response, $args) use ($log) {
    $username = $request->getParam('username');
    $password = $request->getParam('password');

    $record = DB::queryFirstRow("SELECT id,username,email,password FROM users WHERE username=%s", $username); //This "WHERE" originally checked the $email
    $loginSuccess = false;
    if ($record) {
        global $passwordPepper;
        $pwdPeppered = hash_hmac("sha256", $password, $passwordPepper);
        $pwdHashed = $record['password'];
        if (password_verify($pwdPeppered, $pwdHashed)) {
            $loginSuccess = true;
        }
        // WARNING: only temporary solution to allow for old plain-text passwords to continue to work
        // Plain text passwords comparison
        else if ($record['password'] == $password) {
            $loginSuccess = true;
        }
    }

    //If login success is true, let's log in
    if (!$loginSuccess) {
        $log->info(sprintf("Login failed for user %s from %s", $username, $_SERVER['REMOTE_ADDR']));
        return $this->view->render($response, 'login_failed.html.twig', ['error' => true]);
    } else {
        unset($record['password']); // for security reasons remove password from session
        $_SESSION['user'] = $record; // remember user logged in
        $log->debug(sprintf("Login successful for user %s, id=%d, from %s", $username, $record['id'], $_SERVER['REMOTE_ADDR']));
        $returnUrl = $request->getParam('returnUrl', "");
        // TODO: Sanitize URL - refuse to use it if invalid, e.g. check if it begins with '/'   /viewitem/234
        if ($returnUrl) {
            setFlashMessage("You have logged in");
            return $response->withRedirect($returnUrl, 301);
        } else {
            setFlashMessage("Login successful");
            return $response->withRedirect("/");
        }
    }
});

////////////////////////////////////////////////////////////////////////
// LOG OUT
////////////////////////////////////////////////////////////////////////
$app->get('/logout', function ($request, $response, $args) use ($log) {
    $log->debug(sprintf("Logout successful for id=%d, from %s", @$_SESSION['user']['id'], $_SERVER['REMOTE_ADDR']));
    unset($_SESSION['user']);
    setFlashMessage("You have logged out successfully");
    return $response->withRedirect("/");
});

///////////////////////////////////////////////////////
//THE FUNCTIONS
///////////////////////////////////////////////////////

function verifyUserName($name)
{
    if (preg_match('/^[a-zA-Z0-9\ \\._\'"-]{4,50}$/', $name) != 1) { // no match
        return "Name must be 4-50 characters long and consist of letters, digits, "
            . "spaces, dots, underscores, apostrophies, or minus sign.";
    }
    return TRUE;
}

function verifyPasswordQuailty($pass1, $pass2)
{
    if ($pass1 != $pass2) {
        return "Passwords do not match";
    } else {
        /*
        // FIXME: figure out how to use case-sensitive regexps with Validator
        if (!Validator::length(6,100)->regex('/[A-Z]/')->validate($pass1)) {
            return "VALIDATOR. Password must be 6-100 characters long, "
                . "with at least one uppercase, one lowercase, and one digit in it";
        } */
        if ((strlen($pass1) < 6) || (strlen($pass1) > 100)
            || (preg_match("/[A-Z]/", $pass1) == FALSE)
            || (preg_match("/[a-z]/", $pass1) == FALSE)
            || (preg_match("/[0-9]/", $pass1) == FALSE)
        ) {
            return "Password must be 6-100 characters long, "
                . "with at least one uppercase, one lowercase, and one digit in it";
        }
    }
    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////
// ONLY IF THERE IS AN IMAGE
// USED IN PICTURES.PHP
// function verifyUploadedPhoto(Psr\Http\Message\UploadedFileInterface $photo, &$mime = null)
// {
//     if ($photo->getError() != 0) {
//         return "Error uploading photo " . $photo->getError();
//     }
//     if ($photo->getSize() > 1024 * 1024) { // 1MiB
//         return "File too big. 1MB max is allowed.";
//     }
//     $info = getimagesize($photo->file);
//     if (!$info) {
//         return "File is not an image";
//     }
//     // echo "\n\nimage info\n";
//     // print_r($info);
//     if ($info[0] < 200 || $info[0] > 1000 || $info[1] < 200 || $info[1] > 1000) {
//         return "Width and height must be within 200-1000 pixels range";
//     }
//     $ext = "";
//     switch ($info['mime']) {
//         case 'image/jpeg':
//             $ext = "jpg";
//             break;
//         case 'image/gif':
//             $ext = "gif";
//             break;
//         case 'image/png':
//             $ext = "png";
//             break;
//         default:
//             return "Only JPG, GIF and PNG file types are allowed";
//     }
//     if (!is_null($mime)) {
//         $mime = $info['mime'];
//     }
//     return TRUE;
// }

function setFlashMessage($message)
{
    $_SESSION['flashMessage'] = $message;
}

// returns empty string if no message, otherwise returns string with message and clears is
function getAndClearFlashMessage()
{
    if (isset($_SESSION['flashMessage'])) {
        $message = $_SESSION['flashMessage'];
        unset($_SESSION['flashMessage']);
        return $message;
    }
    return "";
}

// used via AJAX -- This is from the register.html.twig AJAX, it verifies if the email is taken
$app->get('/isemailtaken/[{email}]', function ($request, $response, $args) {
    $email = isset($args['email']) ? $args['email'] : "";
    $record = DB::queryFirstRow("SELECT id FROM users WHERE email=%s", $email);
    if ($record) {
        return $response->write("Email already in use");
    } else {
        return $response->write("");
    }
});
