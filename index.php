<?php

require_once 'vendor/autoload.php';

use Slim\Http\Request;
use Slim\Http\Response;

require_once 'init.php';

require_once 'users.php';

require_once 'pictures.php';

$app->get('/', function ($request, $response, $args) {
    return $this->view->render($response, 'index.html.twig');
});


$app->run();
